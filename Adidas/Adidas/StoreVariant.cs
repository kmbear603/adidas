﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adidas
{
    internal class StoreVariant
    {
        public string Store;
        public string Size;
        public string SizeUnit;
        public int Quantity;
        public string Currency;
        public string Dollar;
        public float Price;
        public float? PriceSale;
        public string ProductUrl;
        public string Name;
        public string Gender;
        public string AddToCartUrl;

        private bool _InStock = false;
        public bool InStock
        {
            get { return _InStock || Quantity > 0; }
            set { _InStock = value; }
        }

        public override string ToString()
        {
            return Store + "/" + SizeUnit + "/" + Size + "/" + Quantity + "/" + Price;
        }
    }
}
