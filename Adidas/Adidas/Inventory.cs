﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adidas
{
    internal class Inventory
    {
        public string ProductId;
        public Variant[] Variants;
        public string ImageUrl;

        public override string ToString()
        {
            return ProductId + " [" + (Variants != null ? Variants.Length : 0).ToString() + "]";
        }

        public int Total
        {
            get
            {
                if (Variants == null)
                    return 0;

                int count = 0;

                foreach (Variant variant in Variants)
                {
                    if (variant.Stores == null)
                        continue;

                    foreach (StoreVariant store_variant in variant.Stores)
                        count += store_variant.Quantity;
                }

                return count;
            }
        }

        public int CountQuantityOfStore(string store_id)
        {
            if (Variants == null)
                return 0;

            int count = 0;
            foreach (Variant variant in Variants)
            {
                if (variant.Stores == null)
                    continue;

                StoreVariant store_variant = Array.Find(variant.Stores, sv =>
                {
                    return sv.Store == store_id;
                });
                if (store_variant == null)
                    continue;
                count += store_variant.Quantity;
            }

            return count;
        }

        public string GetCurrency(string store_id)
        {
            if (Variants == null)
                return null;

            foreach (Variant variant in Variants)
            {
                if (variant.Stores == null)
                    continue;

                StoreVariant store_variant = Array.Find(variant.Stores, sv =>
                {
                    return sv.Store == store_id;
                });
                if (store_variant == null)
                    continue;
                return store_variant.Currency;
            }

            return null;
        }

        public float? GetSalePriceOfStore(string store_id)
        {
            if (Variants == null)
                return null;

            foreach (Variant variant in Variants)
            {
                if (variant.Stores == null)
                    continue;

                StoreVariant store_variant = Array.Find(variant.Stores, sv =>
                {
                    return sv.Store == store_id;
                });
                if (store_variant == null)
                    continue;
                return store_variant.PriceSale;
            }

            return null;
        }

        public float? GetPriceOfStore(string store_id)
        {
            if (Variants == null)
                return null;

            foreach (Variant variant in Variants)
            {
                if (variant.Stores == null)
                    continue;

                StoreVariant store_variant = Array.Find(variant.Stores, sv =>
                {
                    return sv.Store == store_id;
                });
                if (store_variant == null)
                    continue;
                return store_variant.Price;
            }

            return null;
        }
    }
}
