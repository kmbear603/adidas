﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Adidas
{
    internal class ProductUI
    {
        private string _ProductId = "";
        private bool _Loading = true;
        private bool _Error = false;
        private bool _Notify = false;
        private Inventory _Inventory = null;

        private Panel _Panel = null;

        // header
        private Panel _HeaderPanel = null;
        private Label _ProductIdLabel = null;
        private LinkLabel _RefreshLinkLabel = null;
        private LinkLabel _RemoveLinkLabel = null;

        // content
        private Panel _ContentPanel = null;
        private PictureBox _PictureBox = null;
        private ListView _SummaryListview = null;
        private ListView _AvailabilityListview = null;

        public delegate void OnRemove_D(ProductUI ui);
        public event OnRemove_D OnRemove = null;

        public delegate void OnRefresh_D(ProductUI ui);
        public event OnRefresh_D OnRefresh = null;

        public ProductUI(string product_id)
        {
            _ProductId = product_id;

            _Panel = new Panel();
            _Panel.Tag = this;
            _Panel.BorderStyle = BorderStyle.FixedSingle;
            _Panel.BackColor = Color.White;
            _Panel.ForeColor = Color.Black;

            _HeaderPanel = new Panel();
            _HeaderPanel.BorderStyle = BorderStyle.FixedSingle;
            _HeaderPanel.BackColor = Color.FromArgb(192, 224, 224);
            _HeaderPanel.ForeColor = Color.Black;

            _ProductIdLabel = new Label();
            _ProductIdLabel.Font = new Font(_HeaderPanel.Font, FontStyle.Bold);
            _ProductIdLabel.Text = product_id;
            _ProductIdLabel.AutoSize = true;
            _HeaderPanel.Controls.Add(_ProductIdLabel);

            _RefreshLinkLabel = new LinkLabel();
            _RefreshLinkLabel.Text = "refresh";
            _RefreshLinkLabel.AutoSize = true;
            _RefreshLinkLabel.LinkClicked += RefreshLinkLabel_LinkClicked;
            _HeaderPanel.Controls.Add(_RefreshLinkLabel);

            _RemoveLinkLabel = new LinkLabel();
            _RemoveLinkLabel.Text = "remove";
            _RemoveLinkLabel.AutoSize = true;
            _RemoveLinkLabel.LinkClicked += RemoveLinkLabel_LinkClicked;
            _HeaderPanel.Controls.Add(_RemoveLinkLabel);

            _Panel.Controls.Add(_HeaderPanel);

            _ContentPanel = new Panel();
            _ContentPanel.Paint += ContentPanel_Paint;

            _PictureBox = new PictureBox();
            _PictureBox.BorderStyle = BorderStyle.FixedSingle;
            _PictureBox.SizeMode = PictureBoxSizeMode.Zoom;
            _ContentPanel.Controls.Add(_PictureBox);

            _SummaryListview = new ListView()
            {
                FullRowSelect = true,
                GridLines = true,
                View = View.Details,
            };
            _SummaryListview.Columns.Add("", 0);
            _SummaryListview.Columns.Add("", 50, HorizontalAlignment.Center);
            _SummaryListview.Columns.Add("", 50, HorizontalAlignment.Center);
            _SummaryListview.Columns.Add("", 190, HorizontalAlignment.Center);
            _ContentPanel.Controls.Add(_SummaryListview);

            _AvailabilityListview = new ListView()
            {
                GridLines = true,
                FullRowSelect = true,
                View = View.Details,
                Scrollable = false
            };
            _ContentPanel.Controls.Add(_AvailabilityListview);

            _Panel.Controls.Add(_ContentPanel);

            SetLoading(true);

            _Panel.Resize += Panel_Resize;
        }

        private void Panel_Resize(object sender, EventArgs e)
        {
            int padding = 10;

            // header begin

            _HeaderPanel.Left = _HeaderPanel.Top = 0;
            _HeaderPanel.Width = _Panel.DisplayRectangle.Width;

            _ProductIdLabel.Font = new Font(_HeaderPanel.Font, FontStyle.Bold);
            _ProductIdLabel.Left = padding;
            _ProductIdLabel.Top = padding;

            _RemoveLinkLabel.Left = _HeaderPanel.Width - padding - _RemoveLinkLabel.Width;
            _RemoveLinkLabel.Top = _ProductIdLabel.Bottom - _RemoveLinkLabel.Height;

            _RefreshLinkLabel.Left = _RemoveLinkLabel.Left - padding - _RefreshLinkLabel.Width;
            _RefreshLinkLabel.Top = _ProductIdLabel.Bottom - _RefreshLinkLabel.Height;

            _HeaderPanel.Height = _ProductIdLabel.Bottom + padding;

            // header end

            // content begin

            _ContentPanel.Left = 0;
            _ContentPanel.Top = _HeaderPanel.Bottom;
            _ContentPanel.Width = _Panel.DisplayRectangle.Width;
            _ContentPanel.Height = _Panel.DisplayRectangle.Height - _ContentPanel.Top;

            _PictureBox.Left = _PictureBox.Top = 0;
            _PictureBox.Width = _PictureBox.Height = 160;

            _SummaryListview.Left = _PictureBox.Right;
            _SummaryListview.Top = 0;
            _SummaryListview.Width = _ContentPanel.Width - _SummaryListview.Left;
            _SummaryListview.Height = _PictureBox.Height;

            _AvailabilityListview.Left = 0;
            _AvailabilityListview.Top = _PictureBox.Bottom;
            _AvailabilityListview.Width = _ContentPanel.Width;
            _AvailabilityListview.Height = _ContentPanel.Height - _AvailabilityListview.Top;

            // content end
        }

        private void ContentPanel_Paint(object sender, PaintEventArgs e)
        {
            string txt;
            if (_Loading)
                txt = "Loading...";
            else if (_Error)
                txt = "Error";
            else
                return;

            Font font = new Font(_ContentPanel.Font.FontFamily, _ContentPanel.Font.Size + 3.0f);

            Graphics g = e.Graphics;
            SizeF size = g.MeasureString(txt, font);

            g.DrawString(
                txt, font, new SolidBrush(_ContentPanel.ForeColor),
                new RectangleF((_ContentPanel.Width - size.Width) / 2, (_ContentPanel.Height - size.Height) / 2, size.Width, size.Height));
        }

        private void RemoveLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (OnRemove != null)
                OnRemove(this);
        }

        private void RefreshLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (OnRefresh != null)
                OnRefresh(this);
        }

        public string ProductId
        {
            get { return _ProductId; }
        }

        public bool Notify
        {
            get { return _Notify; }
        }

        public void SetNotify(bool notify)
        {
            _Notify = notify;
        }

        public bool IsLoading
        {
            get { return _Loading; }
        }

        public void SetLoading(bool loading)
        {
            Func<bool> work = () =>
            {
                _Loading = loading;
                _PictureBox.Visible = _SummaryListview.Visible = _AvailabilityListview.Visible = !_Loading;
                _ContentPanel.Invalidate();
                return true;
            };

            if (_Panel.Parent == null)
                work();
            else {
                _Panel.Invoke((MethodInvoker)delegate ()
                {
                    work();
                });
            }
        }

        public void SetError(bool error)
        {
            _Panel.Invoke((MethodInvoker)delegate ()
            {
                _Error = error;
                _PictureBox.Visible = _SummaryListview.Visible = _AvailabilityListview.Visible = !_Error;
                _ContentPanel.Invalidate();
            });
        }

        public Inventory GetInventory()
        {
            return _Inventory;
        }

        public void SetInventory(Inventory inventory, string[] store_ids)
        {
            Func<string, string, string, ListViewItem> add_summary_row = (key, val1, val2) =>
            {
                return _SummaryListview.Items.Add(new ListViewItem(new string[]
                {
                    "", key, val1, val2
                })
                {
                    UseItemStyleForSubItems = false
                });
            };

            _Panel.Invoke((MethodInvoker)delegate ()
            {
                _PictureBox.LoadAsync(inventory.ImageUrl);

                _SummaryListview.Items.Clear();
                add_summary_row("Total", inventory.Total.ToString(), "");
                foreach (string store_id in store_ids)
                {
                    int quantity = inventory.CountQuantityOfStore(store_id);
                    if (quantity == 0)
                        continue;

                    float? price = inventory.GetPriceOfStore(store_id);
                    float? sale_price = inventory.GetSalePriceOfStore(store_id);
                    string currency = inventory.GetCurrency(store_id);

                    string price_str;
                    if (price == null)
                        price_str = "";
                    else
                    {
                        if (sale_price != null && sale_price.Value != price.Value)
                            price_str = currency + " " + sale_price.Value.ToString("f2") + " from " + price.Value.ToString("f2");
                        else
                            price_str = currency + " " + price.Value.ToString("f2");
                    }

                    ListViewItem itm = add_summary_row(store_id.ToUpper(), quantity.ToString(), price_str);
                    if (sale_price != null && sale_price.Value != price.Value)
                        itm.SubItems[3].ForeColor = Color.Red;
                }

                Dictionary<string, int> column_map = new Dictionary<string, int>();
                _AvailabilityListview.Clear();
                _AvailabilityListview.Columns.Add("", 0);
                foreach (string store_id in store_ids)
                {
                    column_map.Add(store_id, _AvailabilityListview.Columns.Count);
                    _AvailabilityListview.Columns.Add(store_id.ToUpper(), 82, HorizontalAlignment.Center);
                }

                if (inventory.Variants != null)
                {
                    foreach (Variant variant in inventory.Variants)
                    {
                        if (variant.Stores == null)
                            continue;

                        int all_store_count = 0;
                        ListViewItem itm = new ListViewItem(new string[_AvailabilityListview.Columns.Count]);

                        foreach (StoreVariant store_variant in variant.Stores)
                        {
                            if (store_variant.Quantity == 0)
                                continue;

                            int col = column_map[store_variant.Store];
                            itm.SubItems[col].Text = store_variant.Size + " [" + store_variant.Quantity.ToString() + "]";

                            all_store_count += store_variant.Quantity;
                        }

                        if (all_store_count > 0)
                            _AvailabilityListview.Items.Add(itm);
                    }
                }

                if (_AvailabilityListview.Items.Count > 0)
                {
                    Rectangle rect = _AvailabilityListview.GetItemRect(_AvailabilityListview.Items.Count - 1);
                    _AvailabilityListview.Height = rect.Bottom + 2;
                }
                else
                    _AvailabilityListview.Height = 2;

                _ContentPanel.Height = _AvailabilityListview.Bottom;
                _Panel.Height = _ContentPanel.Bottom;

                _Inventory = inventory;
            });
        }

        public Panel Panel
        {
            get { return _Panel; }
        }
    }
}
