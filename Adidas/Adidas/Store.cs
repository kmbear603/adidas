﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Adidas
{
    internal class Store
    {
        public string Id;
        public string Name;
        public string Url;
        public string VariantUrl;
        public string AddCartUrl;
        public string MediaDomain;
        public string Currency;
        public string Dollar;
        public string SizeUnit;
        public Dictionary<string, string> Pages;
        public string ProductUrl;   // for JP

        public override string ToString()
        {
            return Name;
        }

        public string GetVariantUrl(string product_id)
        {
            return VariantUrl + "?pid=" + product_id;
        }

        public string GetAddCartUrl(string variant_id)
        {
            return GetAddCartUrl(variant_id, false);
        }

        public string GetAddCartUrl(string variant_id, bool really_add)
        {
            return AddCartUrl + "?pid=" + variant_id + "&Quantity=1&ajax=true" + (really_add ? "&layer=Add%20To%20Bag%20overlay" : "");
        }

        public string GetImageUrl(string product_id)
        {
            return MediaDomain + "/" + product_id + "_01_standard.jpg?sw=400&sfrm=jpg";
        }

        public static Store[] LoadFromFile(string file_name)
        {
            List<Store> list = new List<Store>();

            string txt;
            using (StreamReader sr = new StreamReader(file_name))
                txt = sr.ReadToEnd();

            Newtonsoft.Json.Linq.JArray root = (Newtonsoft.Json.Linq.JArray)Newtonsoft.Json.JsonConvert.DeserializeObject(txt);
            foreach (Newtonsoft.Json.Linq.JToken store_obj in root)
            {
                if (store_obj.Value<bool>("hidden"))
                    continue;

                Dictionary<string, string> pages = new Dictionary<string, string>();
                Newtonsoft.Json.Linq.JObject pages_obj = store_obj.Value<Newtonsoft.Json.Linq.JObject>("pages");
                foreach (Newtonsoft.Json.Linq.JProperty prop in pages_obj.Properties())
                    pages.Add(prop.Name, pages_obj.Value<string>(prop.Name));

                list.Add(new Store()
                {
                    Id = store_obj.Value<string>("id"),
                    Name = store_obj.Value<string>("name"),
                    Url = store_obj.Value<string>("url"),
                    VariantUrl = store_obj.Value<string>("variantUrl"),
                    MediaDomain = store_obj.Value<string>("mediaDomain"),
                    Currency = store_obj.Value<string>("currency"),
                    Dollar = store_obj.Value<string>("dollar"),
                    SizeUnit = store_obj.Value<string>("sizeUnit"),
                    Pages = pages,
                    AddCartUrl = store_obj.Value<string>("addCartUrl"),
                    ProductUrl = (store_obj["productUrl"] != null ? store_obj.Value<string>("productUrl") : null)
                });
            }

            return list.ToArray();
        }
    }
}
