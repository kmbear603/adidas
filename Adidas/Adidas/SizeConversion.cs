﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adidas
{
    internal static class SizeConversion
    {
        private class Info
        {
            public string Name;
            public float Min;
            public float Max;
            public float Step;

            public Info(string name, float min, float max, float step)
            {
                Name = name;
                Min = min;
                Max = max;
                Step = step;
            }
        }

        private static Info Adidas = new Info("Adidas", 530f, 740f, 10f);
        private static Info US_M = new Info("US_M", 4f, 14.5f, 0.5f);
        private static Info US_W = new Info("US_W", 5f, 13f, 0.5f);
        private static Info UK = new Info("UK", 3.5f, 14f, 0.5f);
        private static Info EU = new Info("EU", 36f, 50f, 2f / 3);
        private static Info JP = new Info("JP", 22f, 32.5f, 0.5f);
        private static Info KR = new Info("KR", 220f, 325f, 5f);

        private static Info[] _Config = new Info[]
        {
            Adidas, US_M, US_W, UK, EU, JP, KR
        };
        private static List<Dictionary<string/*name*/, string/*size string*/>> _Table = null;

        private static void PopulateTable()
        {
            if (_Table != null)
                return;

            _Table = new List<Dictionary<string, string>>();

            Func<Info, int, string> calc = (obj, i) =>
            {
                float v = obj.Min + (i * obj.Step);
                if (v > obj.Max)
                    return null;

                if (obj.Name == "EU" && v - Math.Floor(v) > 0)
                {
                    float r = v - (float)Math.Floor(v);
                    if (r < 0.5)    // 1/3
                        return Math.Floor(v).ToString() + " 1/3";
                    else    // 2/3
                        return Math.Floor(v).ToString() + " 2/3";
                }
                return v.ToString();
            };

            for (int i = 0; i < 500; i++)
            {
                string code = calc(Adidas, i);
                if (code == null)
                    break;

                Dictionary<string, string> sub_table = new Dictionary<string, string>();
                sub_table.Add(Adidas.Name, code);

                foreach (Info info in _Config)
                {
                    if (info.Name == Adidas.Name)
                        continue;
                    string v = calc(info, i);
                    if (v == null)
                        continue;
                    sub_table.Add(info.Name, v);
                }

                _Table.Add(sub_table);
            }
        }

        public static Dictionary<string, string> LookupJPSize(float jp_size)
        {
            PopulateTable();

            return _Table.Find(sub =>
            {
                return jp_size == float.Parse(sub[JP.Name]);
            });
        }

        public static Dictionary<string, string> LookupKRSize(int kr_size)
        {
            PopulateTable();

            return _Table.Find(sub =>
            {
                return kr_size == int.Parse(sub[KR.Name]);
            });
        }
    }
}
