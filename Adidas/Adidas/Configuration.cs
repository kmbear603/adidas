﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;

namespace Adidas
{
    internal class Configuration
    {
        private Dictionary<string, string> _Table = new Dictionary<string, string>();

        public Configuration()
        {
        }

        public void Load(string file_name)
        {
            _Table.Clear();

            if (!File.Exists(file_name))
                return;

            XmlDocument xd = new XmlDocument();
            xd.Load(file_name);

            XmlNode root = xd.SelectSingleNode("config");
            foreach (XmlNode node in root.ChildNodes)
                _Table.Add(node.Name, node.InnerText);
        }

        public void Save(string file_name)
        {
            XmlDocument xd = new XmlDocument();
            XmlNode root = xd.CreateElement("config");
            foreach (string key in _Table.Keys)
            {
                XmlNode node = xd.CreateElement(key);
                node.InnerText = _Table[key];
                root.AppendChild(node);
            }
            xd.AppendChild(root);
            xd.Save(file_name);
        }

        private string Get(string key, string default_val)
        {
            if (_Table.ContainsKey(key))
                return _Table[key];
            return default_val;
        }

        private void Set(string key, string val)
        {
            if (_Table.ContainsKey(key))
                _Table.Remove(key);
            _Table.Add(key, val);
        }

        public string SenderEmailAddress
        {
            get { return Get("SenderEmail", ""); }
            set { Set("SenderEmail", value); }
        }

        public string SenderEmailPassword
        {
            get { return Get("SenderEmailPassword", ""); }
            set { Set("SenderEmailPassword", value); }
        }

        public string[] NotificationEmails
        {
            get
            {
                string str = Get("NotificationEmails", "");
                if (str == "")
                    return new string[0];
                return str.Split(':');
            }
            set
            {
                string str = string.Join(":", value);
                Set("NotificationEmails", str);
            }
        }

        public string[] NotificationProducts
        {
            get
            {
                string str = Get("NotificationProducts", "");
                if (str == "")
                    return new string[0];
                return str.Split(',');
            }
            set
            {
                string str = string.Join(",", value);
                Set("NotificationProducts", str);
            }
        }

        public string[] DisplayProducts
        {
            get
            {
                string str = Get("DisplayProducts", "");
                if (str == "")
                    return new string[0];
                return str.Split(',');
            }
            set
            {
                string str = string.Join(",", value);
                Set("DisplayProducts", str);
            }
        }
    }
}
