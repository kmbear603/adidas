﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

namespace Adidas
{
    internal static class SimpleHttp
    {
        public static Task<string> GetTextAsync(string url)
        {
            return GetTextAsync(url, TimeSpan.FromSeconds(30));
        }

        public static async Task<string> GetTextAsync(string url, TimeSpan timeout)
        {
            using (HttpClient client = new HttpClient(new HttpClientHandler()
            {
                //AllowAutoRedirect = false
                AutomaticDecompression = System.Net.DecompressionMethods.GZip | System.Net.DecompressionMethods.Deflate
            }))
            {
                if (timeout != TimeSpan.MaxValue)
                    client.Timeout = timeout;

                client.DefaultRequestHeaders.Add("Accept", "*/*");
                client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
                client.DefaultRequestHeaders.Add("Connection", "keep-alive");

                HttpResponseMessage res = await client.GetAsync(url).ConfigureAwait(false);
                Console.WriteLine(res.StatusCode);
                res.EnsureSuccessStatusCode();
                return await res.Content.ReadAsStringAsync();
            }
        }
    }
}
