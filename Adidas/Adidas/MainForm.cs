﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Adidas
{
    internal partial class MainForm : Form
    {
        private const string CONFIG_FILE_NAME = "config.xml";
        private Configuration _Config = null;
        private Adidas _Adidas = null;
        private bool _Started = false;
        private AutoResetEvent _ExitEvent = null;
        private Thread _MonitorThread = null;
        private const int COLUMN_WIDTH = 500;

        public MainForm()
        {
            InitializeComponent();

            _Config = new Configuration();
            _Config.Load(CONFIG_FILE_NAME);

            _Adidas = new Adidas();
        }

        ~MainForm()
        {
            _Adidas.Dispose();
            _Adidas = null;

            _Config = null;
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            this.Enabled = false;

            try
            {
                if (!_Started)
                {
                    _Config.SenderEmailAddress = SenderGmailTextBox.Text = SenderGmailTextBox.Text.Trim();
                    _Config.SenderEmailPassword = SenderGmailPasswordTextBox.Text.Trim();
                    SenderGmailPasswordTextBox.Text = "".PadLeft(_Config.SenderEmailPassword.Length, '*');

                    if (_Config.SenderEmailAddress == "" || _Config.SenderEmailPassword == "")
                    {
                        MessageBox.Show("email and password cannoe be empty");
                        return;
                    }

                    List<string> notitication_emails = new List<string>();
                    foreach (string line in NotificationListTextBox.Lines)
                    {
                        if (line.Trim() == "")
                            continue;
                        notitication_emails.Add(line.Trim());
                    }
                    _Config.NotificationEmails = notitication_emails.ToArray();
                    if (_Config.NotificationEmails.Length == 0)
                    {
                        MessageBox.Show("No notification email");
                        return;
                    }

                    _Config.Save(CONFIG_FILE_NAME);

                    _ExitEvent = new AutoResetEvent(false);
                    _MonitorThread = new Thread(new ThreadStart(MonitorThread));
                    _MonitorThread.Start();

                    SenderGmailTextBox.Enabled = SenderGmailPasswordTextBox.Enabled = NotificationListTextBox.Enabled = false;
                    StartButton.Text = "Stop notification";
                    StartButton.BackColor = Color.LightPink;

                    _Started = true;
                }
                else
                {
                    _ExitEvent.Set();

                    _MonitorThread.Join();
                    _MonitorThread = null;

                    _ExitEvent.Close();
                    _ExitEvent = null;

                    SenderGmailTextBox.Enabled = SenderGmailPasswordTextBox.Enabled = NotificationListTextBox.Enabled = true;
                    SenderGmailPasswordTextBox.Text = _Config.SenderEmailPassword;
                    StartButton.Text = "Start notification";
                    StartButton.BackColor = Color.PaleGreen;

                    _Started = false;
                }
            }
            finally
            {
                this.Enabled = true;
            }
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            DoResize();
        }

        private void DoResize()
        {
            int padding = 5;

            RefreshLinkLabel.Left = DisplayRectangle.Width - padding - RefreshLinkLabel.Width;
            ProductContainerPanel.Width = DisplayRectangle.Width - padding - ProductContainerPanel.Left;
            ProductContainerPanel.Height = DisplayRectangle.Height - padding - ProductContainerPanel.Top;

            RelayoutProductContainerPanel();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            DoResize();

            SenderGmailTextBox.Text = _Config.SenderEmailAddress;
            SenderGmailPasswordTextBox.Text = _Config.SenderEmailPassword;
            NotificationListTextBox.Lines = _Config.NotificationEmails;

            foreach (string product_id in _Config.DisplayProducts)
                NewProductUI(product_id);

            RelayoutProductContainerPanel();
        }

        private ProductUI NewProductUI(string product_id)
        {
            ProductUI ui = new ProductUI(product_id);
            //ui.SetNotify(Array.FindIndex(_Config.NotificationProducts, p => { return p == product_id; }) != -1);
            ui.SetNotify(true);
            ui.OnRefresh += ProductUI_OnRefresh;
            ui.OnRemove += ProductUI_OnRemove;

            ProductContainerPanel.Controls.Add(ui.Panel);
            ui.Panel.Width = COLUMN_WIDTH;
            ui.Panel.Height = 300;

            RefreshProduct(ui);

            return ui;
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void AddProductButton_Click(object sender, EventArgs e)
        {
            string product_id = ProductIdTextBox.Text = ProductIdTextBox.Text.Trim().ToUpper();

            int idx = Array.FindIndex(_Config.DisplayProducts, p => { return p == product_id; });
            if (idx != -1)
            {
                ProductIdTextBox.SelectAll();
                MessageBox.Show("already exist");
                return;
            }

            NewProductUI(product_id);

            _Config.DisplayProducts = AppendToArray(product_id, _Config.DisplayProducts);
            _Config.Save(CONFIG_FILE_NAME);

            ProductIdTextBox.Text = "";
            ProductIdTextBox.SelectAll();

            RelayoutProductContainerPanel();
        }

        private void RelayoutProductContainerPanel()
        {
            this.BeginInvoke((MethodInvoker)delegate ()
            {
                if (ProductContainerPanel.Controls.Count == 0)
                    return;

                int padding = 20;

                List<int> col_bottom = new List<int>();
                int col_count = 0;
                int left = 0;
                do
                {
                    left += COLUMN_WIDTH + padding;
                    col_count++;
                }
                while (left + COLUMN_WIDTH <= ProductContainerPanel.Width);

                for (int i = 0; i < col_count; i++)
                    col_bottom.Add(ProductContainerPanel.AutoScrollPosition.Y);

                foreach (Control control in ProductContainerPanel.Controls)
                {
                    int shortest_col = -1;
                    for (int i = 0; i < col_bottom.Count; i++)
                    {
                        if (i == 0 || col_bottom[i] < col_bottom[shortest_col])
                            shortest_col = i;
                    }

                    control.Left = ((COLUMN_WIDTH + padding) * shortest_col);
                    control.Top = col_bottom[shortest_col];

                    col_bottom[shortest_col] = control.Bottom + padding;
                }
            });
        }

        private static string[] AppendToArray(string val, string[] arr)
        {
            string[] new_arr = new string[arr.Length + 1];
            Array.Copy(arr, new_arr, arr.Length);
            new_arr[new_arr.Length - 1] = val;
            return new_arr;
        }

        private static string[] RemoveFromArray(int idx, string[] arr)
        {
            if (arr.Length == 1 && idx == 0)
                return new string[0];

            string[] new_arr = new string[arr.Length - 1];
            Array.Copy(arr, 0, new_arr, 0, idx);
            Array.Copy(arr, idx + 1, new_arr, idx, arr.Length - idx - 1);
            return new_arr;
        }

        private void ProductUI_OnRemove(ProductUI ui)
        {
            List<Panel> remove_list = new List<Panel>();
            foreach (Panel panel in ProductContainerPanel.Controls)
            {
                ProductUI test_ui = panel.Tag as ProductUI;
                if (test_ui.ProductId == ui.ProductId)
                    remove_list.Add(panel);
            }
            if (remove_list.Count == 0)
                return;

            foreach (Panel panel in remove_list)
                ProductContainerPanel.Controls.Remove(panel);

            int idx = Array.FindIndex(_Config.NotificationProducts, p => { return p == ui.ProductId; });
            if (idx != -1)
                _Config.NotificationProducts = RemoveFromArray(idx, _Config.NotificationProducts);

            idx = Array.FindIndex(_Config.DisplayProducts, p => { return p == ui.ProductId; });
            if (idx != -1)
                _Config.DisplayProducts = RemoveFromArray(idx, _Config.DisplayProducts);

            _Config.Save(CONFIG_FILE_NAME);

            RelayoutProductContainerPanel();
        }

        private async void ProductUI_OnRefresh(ProductUI ui)
        {
            await RefreshProduct(ui);
        }

        public Task<Inventory> RefreshProduct(ProductUI ui)
        {
            return Task.Run(async () =>
            {
                try
                {
                    ui.SetLoading(true);
                    Inventory inventory = await _Adidas.GetInventoryAsync(_Adidas.AllStoreIds, ui.ProductId);
                    ui.SetError(false);
                    ui.SetInventory(inventory, _Adidas.AllStoreIds);
                    RelayoutProductContainerPanel();
                    return inventory;
                }
                catch
                {
                    ui.SetError(true);
                    return null;
                }
                finally
                {
                    ui.SetLoading(false);
                }
            });
        }

        private static StoreVariant[] GetNewInventory(Inventory inventory, Inventory reference)
        {
            List<StoreVariant> ret = new List<StoreVariant>();

            if (inventory == null || inventory.Variants == null)
                return new StoreVariant[0];

            foreach (Variant variant in inventory.Variants)
            {
                if (variant.Stores == null)
                    continue;

                Variant reference_variant = reference != null && reference.Variants != null ? Array.Find(reference.Variants, v => { return v.VariantId == variant.VariantId; }) : null;

                foreach (StoreVariant store_variant in variant.Stores)
                {
                    StoreVariant reference_store_variant = reference_variant != null && reference_variant.Stores != null ? Array.Find(reference_variant.Stores, s => { return s.Store == store_variant.Store; }) : null;
                    if (store_variant.Quantity > 0 && (reference_store_variant == null || reference_store_variant.Quantity == 0))
                        ret.Add(store_variant);
                }
            }

            return ret.ToArray();
        }

        private static bool CompareInventory(Inventory old_inv, Inventory new_inv, out StoreVariant[] added, out StoreVariant[] removed)
        {
            added = GetNewInventory(new_inv, old_inv);
            removed = GetNewInventory(old_inv, new_inv);
            return (added != null && added.Length > 0) || (removed != null && removed.Length > 0);
        }

        private void ProductIdTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                AddProductButton.PerformClick();
        }

        private async void RefreshLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            foreach (Panel panel in ProductContainerPanel.Controls)
            {
                ProductUI ui = panel.Tag as ProductUI;
                if (ui == null)
                    continue;
                await RefreshProduct(ui);
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_Started)
            {
                MessageBox.Show("Stop first");
                e.Cancel = true;
            }
        }

        private void MonitorThread()
        {
            string content_table_style = "style=\"width: 100%; border-collapse: collapse;\"";
            string content_table_tr_style = "style=\"border-bottom: 1px solid #ccc; border-top: 1px solid #ccc;\"";
            string content_table_td_style = "style=\"vertical-align: middle; text-align: center; padding: 3px;\"";

            Func<string, string, string, string> change_font = (txt, font_size, font_color) =>
            {
                return "<span style=\"" + (font_size != null ? "font-size: " + font_size + "; " : "")
                    + (font_color != null ? "color: " + font_color : "") + "\">" + txt + "</span>";
            };

            Func<string, string> center_th = (incell_html) =>
            {
                return "<th style=\"text-align:center\">" + incell_html + "</th>";
            };

            Func<string, string> center_td = (incell_html) =>
            {
                return "<td style=\"text-align:center\" " + content_table_td_style + ">" + incell_html + "</td>";
            };

            Dictionary<string, DateTime> last_changed_table = new Dictionary<string, DateTime>();
            DateTime last_check_all = DateTime.MinValue;
            TimeSpan check_all_period = TimeSpan.FromMinutes(2) - TimeSpan.FromSeconds(1);

            while (!_ExitEvent.WaitOne(TimeSpan.FromMinutes(1)))
            {
                List<ProductUI> check_list = new List<ProductUI>();

                bool check_all;
                {
                    while (true)
                    {
                        bool updating = false;
                        DateTime now = DateTime.Now;
                        check_all = now >= last_check_all + check_all_period;
#if DEBUG
                        Console.WriteLine(now.ToString() + " " + last_check_all.ToString() + " " + check_all.ToString());
#else // DEBUG
#if kill
                        using (System.IO.StreamWriter sw = new System.IO.StreamWriter("debug.log", true))
                            sw.WriteLine(now.ToString() + " " + last_check_all.ToString() + " " + check_all.ToString());
#endif // kill
#endif // DEBUG
                        check_list.Clear();

                        foreach (Panel panel in ProductContainerPanel.Controls)
                        {
                            ProductUI ui = panel.Tag as ProductUI;
                            if (ui == null || !ui.Notify)
                                continue;

                            if (check_all
                                || (last_changed_table.ContainsKey(ui.ProductId) && now < last_changed_table[ui.ProductId] + check_all_period))
                                check_list.Add(ui);

                            updating |= ui.IsLoading;
                        }

                        if (updating)
                            Thread.Sleep(10 * 1000);
                        else
                            break;
                    }

                    if (check_list.Count == 0)
                        continue;
                }

                Inventory[] original_inv = (from ui in check_list select ui.GetInventory()).ToArray();

                Task<Inventory>[] tasks;
                {
                    List<Task<Inventory>> temp_tasks = new List<Task<Inventory>>();
                    List<Task<Inventory>> this_tasks = new List<Task<Inventory>>();

                    int cursor = 0;
                    while (cursor < check_list.Count)
                    {
                        Task<Inventory> t = RefreshProduct(check_list[cursor++]);
                        this_tasks.Add(t);
                        temp_tasks.Add(t);

                        if (this_tasks.Count == 5 || cursor == check_list.Count)
                        {
                            Task.WaitAll(this_tasks.ToArray());
                            this_tasks.Clear();
                        }
                    }

                    tasks = temp_tasks.ToArray();
                }

                StringBuilder report = new StringBuilder();
                report.AppendLine("<!doctype html>");
                report.AppendLine("<html>");
#if kill
                report.AppendLine("<head>");
                report.AppendLine("<title>Adidas Inventory Changed!</title>");
                report.AppendLine("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">");
                report.AppendLine("<link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/simplex/bootstrap.min.css\">");
                report.AppendLine("</head>");
#endif // kill
                report.AppendLine("<body style=\"font-family:Arial\">");
#if kill
                report.AppendLine("<div class=\"container-fluid\">");
#else // kill
#if kill
                report.AppendLine("<style>");
                report.AppendLine(".content-table { width: 100%; border-collapse: collapse; }");
                report.AppendLine(".content-table tr { border-bottom: 1px solid #ccc; border-top: 1px solid #ccc; }");
                report.AppendLine(".content-table td { vertical-align: middle; text-align: center; padding: 3px; }");
                report.AppendLine("</style>");
#endif // kill
#endif // kill

                bool need_to_send = false;

                for (int i = 0; i < check_list.Count;i++)
                {
                    ProductUI ui = check_list[i];

                    SortedDictionary<string/*variant id*/, SortedDictionary<string/*store id*/, StoreVariant[]/*[0]=old, [1]=new*/>> table = new SortedDictionary<string, SortedDictionary<string, StoreVariant[]>>();
                    Func<Inventory, bool, bool> populate = (this_inv, is_current) =>
                    {
                        if (this_inv == null)
                            return true;
                        if (this_inv.Variants == null)
                            return true;

                        foreach (Variant variant in this_inv.Variants)
                        {
                            if (variant.Stores == null)
                                continue;

                            foreach (StoreVariant sv in variant.Stores)
                            {
                                SortedDictionary<string, StoreVariant[]> sub_table;
                                if (!table.ContainsKey(variant.VariantId))
                                {
                                    sub_table = new SortedDictionary<string, StoreVariant[]>();
                                    table.Add(variant.VariantId, sub_table);
                                }
                                else
                                    sub_table = table[variant.VariantId];

                                StoreVariant[] arr;
                                if (!sub_table.ContainsKey(sv.Store))
                                {
                                    arr = new StoreVariant[2];
                                    sub_table.Add(sv.Store, arr);
                                }
                                else
                                    arr = sub_table[sv.Store];

                                arr[is_current ? 1 : 0] = sv;
                            }
                        }

                        return true;
                    };

#if kill
                    Inventory old = ui.GetInventory();
                    Inventory inv = null;
                    AutoResetEvent ev = new AutoResetEvent(false);
                    this.BeginInvoke((MethodInvoker)async delegate ()
                    {
                        inv = await RefreshProduct(ui);
                        ev.Set();
                    });
                    ev.WaitOne();
#else // kill
                    Inventory old = original_inv[i];
                    Inventory inv = tasks[i].Result;
#endif // kill

                    populate(old, false);
                    populate(inv, true);

                    bool changed = false;

                    StringBuilder this_sb = new StringBuilder();

#if kill
                    this_sb.AppendLine("<div class=\"row\">");
#else // kill
                    this_sb.AppendLine("<table border=\"0\" style=\"width: 100%; max-width: 500px\">");
                    this_sb.AppendLine("<tbody>");

                    this_sb.AppendLine("<tr>");
                    this_sb.AppendLine("<td colspan=\"2\" align=\"left\"><strong>" + ui.ProductId + "</strong></td>");
                    this_sb.AppendLine("</tr>");

                    this_sb.AppendLine("<tr>");
#endif // kill

                    // image
#if kill
                    this_sb.AppendLine("<div class=\"col-xs-4\">");
#else // kill
                    this_sb.AppendLine("<td width=\"150\">");
#endif // kill
                    if (inv != null || old != null)
                        this_sb.AppendLine("<img src=\"" + (inv != null ? inv.ImageUrl : old.ImageUrl) + "\" width=\"100%\">");
#if kill
                    this_sb.AppendLine("</div>");
#else // kill
                    this_sb.AppendLine("</td>");
#endif // kill

                    // summary table
#if kill
                    this_sb.AppendLine("<div class=\"col-xs-8\">");
#else // kill
                    this_sb.AppendLine("<td width=\"*\" valign=\"top\">");
#endif // kill
#if kill
                    this_sb.AppendLine("<table class=\"content-table\">");
#else // kill
                    this_sb.AppendLine("<table " + content_table_style + ">");
#endif // kill
                    Dictionary<string/*store id*/, int> count_table = new Dictionary<string, int>();
                    this_sb.AppendLine("<tbody>");
                    {
                        int total = 0;
                        Dictionary<string/*store id*/, string> price_table = new Dictionary<string, string>();
                        Dictionary<string/*store id*/, string> url_table = new Dictionary<string, string>();

                        foreach (string variant_id in table.Keys)
                        {
                            SortedDictionary<string, StoreVariant[]> sub_table = table[variant_id];
                            foreach (string store_id in sub_table.Keys)
                            {
                                StoreVariant[] arr = sub_table[store_id];
                                if (arr[1] == null)
                                    continue;

                                int org_count = 0;
                                if (count_table.ContainsKey(store_id))
                                {
                                    org_count = count_table[store_id];
                                    count_table.Remove(store_id);
                                }
                                count_table.Add(store_id, org_count + arr[1].Quantity);

                                if (arr[1].Quantity > 0 && !price_table.ContainsKey(store_id))
                                {
                                    bool sale = arr[1].PriceSale != null && arr[1].PriceSale.Value != arr[1].Price;

                                    string html;
                                    if (sale)
                                        html = change_font(arr[1].Currency, "x-small", "red") + " " + change_font(arr[1].PriceSale.Value.ToString("f2"), null, "red") + " " + change_font("<strike>" + arr[1].Price.ToString("f2") + "</strike>", "small", "gray");
                                    else
                                        html = change_font(arr[1].Currency, "x-small", null) + " " + change_font(arr[1].Price.ToString("f2"), null, null);

                                    price_table.Add(store_id, html);
                                }

                                if (!string.IsNullOrEmpty(arr[1].ProductUrl) && !url_table.ContainsKey(store_id))
                                    url_table.Add(store_id, "<a href=\"" + arr[1].ProductUrl + "\" target=\"_blank\" style=\"text-decoration:none\">Detail</a>");

                                total += arr[1].Quantity;
                            }
                        }

                        this_sb.AppendLine("<tr " + content_table_tr_style + ">" + center_td("Total") + center_td(total.ToString()) + "<td " + content_table_td_style +"></td>");
                        foreach (string store_id in _Adidas.AllStoreIds)
                        {
                            if (!count_table.ContainsKey(store_id))
                                continue;
                            int count = count_table[store_id];
                            if (count == 0)
                                continue;
                            string price = price_table[store_id];
                            string url = url_table.ContainsKey(store_id) ? url_table[store_id] : "";
                            //this_sb.AppendLine("<tr " + content_table_tr_style + ">" + center_td(store_id.ToUpper()) + center_td(count.ToString()) + center_td(price) +"</tr>");
                            this_sb.AppendLine("<tr " + content_table_tr_style + ">" + center_td(store_id.ToUpper()) + center_td(price) + center_td(url) + "</tr>");
                        }
                    }
                    this_sb.AppendLine("</tbody>");
                    this_sb.AppendLine("</table>");
#if kill
                    this_sb.AppendLine("</div>");
#else // kill
                    this_sb.AppendLine("</td>");
#endif // kill

#if kill
                    this_sb.AppendLine("</div>");
#else // kill
                    this_sb.AppendLine("</tr>");
#endif // kill

                    // inventory table
#if kill
                    this_sb.Append("<div class=\"row\">");
                    this_sb.Append("<div class=\"col-xs-12\">");
#else // kill
                    this_sb.Append("<tr>");
                    this_sb.Append("<td colspan=\"2\">");
#endif // kill
#if kill
                    this_sb.AppendLine("<table class=\"content-table\" style=\"table-layout: fixed\">");
#else // kill
                    this_sb.AppendLine("<table " + content_table_style + " style=\"table-layout: fixed\">");
#endif // kill

                    this_sb.AppendLine("<thead>");
                    this_sb.AppendLine("<tr " + content_table_tr_style + ">");
                    this_sb.Append(string.Join("", (from s in _Adidas.AllStoreIds select center_th(s.ToUpper() + (count_table.ContainsKey(s) && count_table[s] > 0 ? " [" + count_table[s].ToString() + "]" : ""))).ToArray()));
                    this_sb.AppendLine("</tr>");
                    this_sb.AppendLine("</thead>");

                    this_sb.AppendLine("<tbody>");

                    foreach (string variant_id in table.Keys)
                    {
                        SortedDictionary<string, StoreVariant[]> sub_table = table[variant_id];

                        bool exist = false;

                        StringBuilder this_row_sb = new StringBuilder();
                        this_row_sb.Append("<tr " + content_table_tr_style + ">");
                        foreach (string store_id in _Adidas.AllStoreIds)
                        {
                            this_row_sb.Append("<td " + content_table_td_style + ">");
                            if (sub_table.ContainsKey(store_id))
                            {
                                StoreVariant[] arr = sub_table[store_id];

                                int old_quantity = arr[0] != null ? arr[0].Quantity : 0;
                                int new_quantity = arr[1] != null ? arr[1].Quantity : 0;
                                bool on = (old_quantity == 0 && new_quantity > 0);
                                bool off = (old_quantity > 0 && new_quantity == 0);
#if DEBUGxx
                                changed = true;
#else // DEBUG
                                changed |= on || off;
#endif // DEBUG

                                if (old_quantity > 0 || new_quantity > 0)
                                {
                                    // show only if there are inventory now or previously

                                    if (new_quantity > 0 && !string.IsNullOrEmpty(arr[1].AddToCartUrl))
                                        this_row_sb.Append("<a href=\"" + arr[1].AddToCartUrl + "\" target=\"_blank\" style=\"text-decoration:none\">");

                                    string color = on ? "blue" : (off ? "gray" : null);
                                    this_row_sb.Append(change_font(new_quantity.ToString(), "large", color) + "<br/>");

                                    string size = arr[0] != null ? arr[0].Size : arr[1].Size;
                                    this_row_sb.Append(change_font(size, "x-small", color) + "<br/>");

                                    if (on)
                                        this_row_sb.Append(change_font("New", "x-small", color));
                                    if (off)
                                        this_row_sb.Append(change_font("Gone", "x-small", color));

                                    if (new_quantity > 0 && !string.IsNullOrEmpty(arr[1].AddToCartUrl))
                                        this_row_sb.Append("</a>");

                                    exist = true;
                                }
                            }
                            this_row_sb.Append("</td>");
                        }
                        this_row_sb.AppendLine("</tr>");

                        if (exist)
                            this_sb.AppendLine(this_row_sb.ToString());
                    }

                    this_sb.AppendLine("</tbody>");
                    this_sb.AppendLine("</table>");
#if kill
                    this_sb.Append("</div>");
                    this_sb.Append("</div>");
#else // kill
                    this_sb.Append("</td>");
                    this_sb.Append("</tr>");
                    this_sb.Append("</tbody>");
                    this_sb.Append("</table>");
#endif // kill
                    this_sb.Append("<br/>");
                    this_sb.Append("<br/>");

                    if (changed)
                    {
                        report.AppendLine(this_sb.ToString());

                        if (last_changed_table.ContainsKey(ui.ProductId))
                            last_changed_table.Remove(ui.ProductId);
                        last_changed_table.Add(ui.ProductId, DateTime.Now);
                    }
                    need_to_send |= changed;
                }

                report.AppendLine("<hr>");
                report.AppendLine("EOD");
#if kill
                report.AppendLine("</div>");
#endif // kill
                report.AppendLine("</body>");
                report.AppendLine("</html>");

                if (need_to_send)
                {
                    Gmail gmail = new Gmail()
                    {
                        GmailId = _Config.SenderEmailAddress,
                        GmailPassword = _Config.SenderEmailPassword,
                        Subject = "Adidas Inventory Changed!",
                        Body = report.ToString(),
                        IsHtml = true,
                        To = _Config.NotificationEmails
                    };

                    for (int retry = 0; retry < 10; retry++)
                    {
                        try
                        {
                            gmail.Send();
                            break;
                        }
                        catch { }
                    }
                }

                if (check_all)
                    last_check_all = DateTime.Now;

                this.BeginInvoke((MethodInvoker)delegate()
                {
                    LastCheckLabel.Text = "Last checked: " + DateTime.Now.ToString("HH:mm:ss");
                    if (need_to_send)
                        LastChangedLabel.Text = "Last changed: " + DateTime.Now.ToString("HH:mm:ss");
                });
            }
        }
    }
}
