﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adidas
{
    internal class Variant
    {
        public string VariantId;
        public StoreVariant[] Stores;

        public override string ToString()
        {
            return VariantId + "[" + (Stores != null ? Stores.Length : 0).ToString() + "]";
        }
    }
}
