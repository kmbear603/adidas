﻿namespace Adidas
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StartButton = new System.Windows.Forms.Button();
            this.SenderGmailTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SenderGmailPasswordTextBox = new System.Windows.Forms.TextBox();
            this.NotificationListTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.ProductIdTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.AddProductButton = new System.Windows.Forms.Button();
            this.ProductContainerPanel = new System.Windows.Forms.Panel();
            this.RefreshLinkLabel = new System.Windows.Forms.LinkLabel();
            this.LastCheckLabel = new System.Windows.Forms.Label();
            this.LastChangedLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // StartButton
            // 
            this.StartButton.BackColor = System.Drawing.Color.PaleGreen;
            this.StartButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.StartButton.Location = new System.Drawing.Point(15, 327);
            this.StartButton.Margin = new System.Windows.Forms.Padding(4);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(204, 51);
            this.StartButton.TabIndex = 0;
            this.StartButton.Text = "Start notification";
            this.StartButton.UseVisualStyleBackColor = false;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // SenderGmailTextBox
            // 
            this.SenderGmailTextBox.Location = new System.Drawing.Point(15, 35);
            this.SenderGmailTextBox.Name = "SenderGmailTextBox";
            this.SenderGmailTextBox.Size = new System.Drawing.Size(204, 25);
            this.SenderGmailTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Gmail";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Gmail Password";
            // 
            // SenderGmailPasswordTextBox
            // 
            this.SenderGmailPasswordTextBox.Location = new System.Drawing.Point(15, 94);
            this.SenderGmailPasswordTextBox.Name = "SenderGmailPasswordTextBox";
            this.SenderGmailPasswordTextBox.Size = new System.Drawing.Size(204, 25);
            this.SenderGmailPasswordTextBox.TabIndex = 4;
            // 
            // NotificationListTextBox
            // 
            this.NotificationListTextBox.Location = new System.Drawing.Point(15, 171);
            this.NotificationListTextBox.Multiline = true;
            this.NotificationListTextBox.Name = "NotificationListTextBox";
            this.NotificationListTextBox.Size = new System.Drawing.Size(204, 122);
            this.NotificationListTextBox.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(137, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Notification Email List";
            // 
            // ProductIdTextBox
            // 
            this.ProductIdTextBox.Location = new System.Drawing.Point(391, 12);
            this.ProductIdTextBox.Name = "ProductIdTextBox";
            this.ProductIdTextBox.Size = new System.Drawing.Size(110, 25);
            this.ProductIdTextBox.TabIndex = 7;
            this.ProductIdTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ProductIdTextBox_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(236, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(149, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Product ID, eg. BA8842";
            // 
            // AddProductButton
            // 
            this.AddProductButton.Location = new System.Drawing.Point(507, 5);
            this.AddProductButton.Name = "AddProductButton";
            this.AddProductButton.Size = new System.Drawing.Size(87, 36);
            this.AddProductButton.TabIndex = 9;
            this.AddProductButton.Text = "Add";
            this.AddProductButton.UseVisualStyleBackColor = true;
            this.AddProductButton.Click += new System.EventHandler(this.AddProductButton_Click);
            // 
            // ProductContainerPanel
            // 
            this.ProductContainerPanel.AutoScroll = true;
            this.ProductContainerPanel.Location = new System.Drawing.Point(239, 47);
            this.ProductContainerPanel.Name = "ProductContainerPanel";
            this.ProductContainerPanel.Size = new System.Drawing.Size(505, 348);
            this.ProductContainerPanel.TabIndex = 10;
            // 
            // RefreshLinkLabel
            // 
            this.RefreshLinkLabel.AutoSize = true;
            this.RefreshLinkLabel.Location = new System.Drawing.Point(691, 24);
            this.RefreshLinkLabel.Name = "RefreshLinkLabel";
            this.RefreshLinkLabel.Size = new System.Drawing.Size(53, 17);
            this.RefreshLinkLabel.TabIndex = 11;
            this.RefreshLinkLabel.TabStop = true;
            this.RefreshLinkLabel.Text = "Refresh";
            this.RefreshLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.RefreshLinkLabel_LinkClicked);
            // 
            // LastCheckLabel
            // 
            this.LastCheckLabel.AutoSize = true;
            this.LastCheckLabel.Location = new System.Drawing.Point(12, 382);
            this.LastCheckLabel.Name = "LastCheckLabel";
            this.LastCheckLabel.Size = new System.Drawing.Size(0, 17);
            this.LastCheckLabel.TabIndex = 12;
            // 
            // LastChangedLabel
            // 
            this.LastChangedLabel.AutoSize = true;
            this.LastChangedLabel.Location = new System.Drawing.Point(12, 412);
            this.LastChangedLabel.Name = "LastChangedLabel";
            this.LastChangedLabel.Size = new System.Drawing.Size(0, 17);
            this.LastChangedLabel.TabIndex = 13;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.LastChangedLabel);
            this.Controls.Add(this.LastCheckLabel);
            this.Controls.Add(this.RefreshLinkLabel);
            this.Controls.Add(this.ProductContainerPanel);
            this.Controls.Add(this.AddProductButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ProductIdTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.NotificationListTextBox);
            this.Controls.Add(this.SenderGmailPasswordTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SenderGmailTextBox);
            this.Controls.Add(this.StartButton);
            this.Font = new System.Drawing.Font("Microsoft JhengHei", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "MainForm";
            this.Text = "Adidas";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.TextBox SenderGmailTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox SenderGmailPasswordTextBox;
        private System.Windows.Forms.TextBox NotificationListTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox ProductIdTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button AddProductButton;
        private System.Windows.Forms.Panel ProductContainerPanel;
        private System.Windows.Forms.LinkLabel RefreshLinkLabel;
        private System.Windows.Forms.Label LastCheckLabel;
        private System.Windows.Forms.Label LastChangedLabel;
    }
}

