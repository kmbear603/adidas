﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Adidas
{
    internal class Adidas : IDisposable
    {
        private Store[] _Stores = null;

        public Adidas()
        {
            _Stores = Store.LoadFromFile("store.json");
        }

        public void Dispose()
        {
        }

        public Store LookupStore(string store_id)
        {
            return Array.Find(_Stores, s => { return s.Id == store_id; });
        }

        public string[] AllStoreIds
        {
            get
            {
                return (from s in _Stores select s.Id).ToArray();
            }
        }

        public Task<Inventory> GetInventoryAsync(string[] store_id, string product_id)
        {
            return Task.Run(() =>
            {
                Task<Inventory>[] tasks = (from s in store_id select GetInventoryAsync(s, product_id)).ToArray();
                Task.WaitAll(tasks);

                Inventory ret = new Inventory()
                {
                    ProductId = product_id,
                    ImageUrl = tasks[0].Result.ImageUrl
                };

                List<Variant> variants = new List<Variant>();

                foreach (Task<Inventory> task in tasks)
                {
                    if (task.Result.Variants.Length > 0)
                        ret.ImageUrl = task.Result.ImageUrl;
                    foreach (Variant variant in task.Result.Variants)
                    {
                        Variant save_variant = variants.Find(v => { return v.VariantId == variant.VariantId; });
                        if (save_variant== null)
                        {
                            save_variant = new Variant() { VariantId = variant.VariantId };
                            variants.Add(save_variant);
                        }

                        StoreVariant[] sv = save_variant.Stores;
                        save_variant.Stores = new StoreVariant[sv != null ? sv.Length + variant.Stores.Length : variant.Stores.Length];
                        if (sv != null)
                            Array.Copy(sv, save_variant.Stores, sv.Length);
                        Array.Copy(variant.Stores, 0, save_variant.Stores, sv != null ? sv.Length : 0, variant.Stores.Length);
                    }
                }

                ret.Variants = variants.ToArray();

                return ret;
            });
        }

        public Task<Inventory> GetInventoryJPAsync(string product_id)
        {
            string store_id = "jp";

            return Task.Run(async () =>
            {
                Store store = LookupStore(store_id);
                if (store == null)
                    throw new Exception("unknown store " + store_id);

                string product_url = store.ProductUrl + product_id;
                string html = await SimpleHttp.GetTextAsync(product_url);

                string[] lines = html.Split('\n');

                // item name is stored in the line
                // _item_name['BY2550'] = "UltraBOOST Uncaged";

                // all info are stored in javascript array _kcod_zaik
                //_kcod_zaik['BY2550'] = new Array();
                //_kcod_zaik_sort['BY2550'] = new Array();
                //_kcod_zaik['BY2550']['27'] = new Array();
                //_kcod_zaik['BY2550']['27']['size_name'] = '22.0cm';
                //_kcod_zaik['BY2550']['27']['size_name2'] = '22.0cm';
                //_kcod_zaik['BY2550']['27']['stock_label'] = '×';
                //_kcod_zaik['BY2550']['27']['cc_stock'] = '0';
                //_kcod_zaik['BY2550']['27']['stock_status'] = 'sold';
                //_kcod_zaik['BY2550']['27']['ec_shop_sold_state'] = 'sold_all';
                //_kcod_zaik_sort['BY2550'][0] = '27';
                //_kcod_zaik['BY2550']['28'] = new Array();
                //_kcod_zaik['BY2550']['28']['size_name'] = '22.5cm';
                //_kcod_zaik['BY2550']['28']['size_name2'] = '22.5cm';
                //_kcod_zaik['BY2550']['28']['stock_label'] = '×';
                //_kcod_zaik['BY2550']['28']['cc_stock'] = '0';
                //_kcod_zaik['BY2550']['28']['stock_status'] = 'sold';
                //_kcod_zaik['BY2550']['28']['ec_shop_sold_state'] = 'sold_all';
                //_kcod_zaik_sort['BY2550'][1] = '28';

                Func<string, char[], string> remove = (str, matches) =>
                {
                    string ret = "";
                    foreach (char c in str)
                    {
                        if (Array.Find(matches, m => { return m == c; }) == -1)
                            ret += c;
                    }
                    return ret;
                };

                Func<string, string[]> extract_content = line =>
                {
                    List<string> ret = new List<string>(); // first few elements are value in brackets, last element is value after equal sign

                    int equal = line.IndexOf('=');
                    if (equal < 0)
                        return null;

                    var cursor = 0;
                    while (true)
                    {
                        int i = line.IndexOf('[', cursor);
                        if (i < 0 || i > equal)
                            break;

                        int j = line.IndexOf(']', i + 1);
                        if (j < 0 || j > equal)
                            break;

                        ret.Add(line.Substring(i + 1, j - i - 1).Trim().Replace("\'", "").Replace("\"", ""));

                        cursor = j + 1;
                    }

                    ret.Add(line.Substring(equal + 1).Trim().Replace("\'", "").Replace("\"", "").Replace(";", ""));

                    return ret.ToArray();
                };

                Dictionary<string, Variant> invs = new Dictionary<string, Variant>();
                string item_name = null;

                foreach (string line in lines)
                {
                    if (line.StartsWith("_item_name["))
                    {
                        string[] tokens = extract_content(line);
                        if (tokens != null && tokens.Length == 2)
                        {
                            string this_product_id = tokens[0];
                            if (string.Compare(this_product_id, product_id) == 0)
                                item_name = tokens[1];
                        }
                    }
                    else if (line.StartsWith("_kcod_zaik["))
                    {
                        string[] tokens = extract_content(line);
                        if (tokens != null && tokens.Length == 4)
                        {
                            string this_product_id = tokens[0];
                            if (string.Compare(this_product_id, product_id) == 0)
                            {
                                string index = tokens[1];
                                string key = tokens[2];
                                string val = tokens[3];

                                Variant inv;
                                if (!invs.ContainsKey(index))
                                {
                                    inv = new Variant();
                                    inv.Stores = new StoreVariant[]
                                    {
                                        new StoreVariant()
                                        {
                                            Store = store_id
                                        }
                                    };
                                    invs.Add(index, inv);
                                }
                                else
                                    inv = invs[index];

                                if (key == "size_name")
                                {
                                    inv.Stores[0].Size = val;
                                    float jp_size = float.Parse(remove(val, new char[]
                                    {
                                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.'
                                    }));
                                    Dictionary<string, string> size_obj = SizeConversion.LookupJPSize(jp_size);
                                    inv.VariantId = this_product_id + "_" + size_obj["Adidas"];
                                }
                                else if (key == "stock_label")
                                {
                                    if (val == "×")
                                        inv.Stores[0].Quantity = 0;
                                    else {
                                        int q;
                                        if (int.TryParse(val, out q))
                                            inv.Stores[0].Quantity = q;
                                        else
                                            inv.Stores[0].InStock = true;
                                    }
                                }
                            }
                        }
                    }
                };

                foreach (Variant inv in invs.Values)
                {
                    inv.Stores[0].Price = ori_price;
                    if (curr_price != ori_price)
                        inv.Stores[0].PriceSale = curr_price;

                    inv.Stores[0].SizeUnit = store.SizeUnit;
                    inv.Stores[0].Currency = store.Currency;
                    inv.Stores[0].Dollar = store.Dollar;
                }

                List<Variant> sorted_invs = new List<Variant>();
                sorted_invs.AddRange(invs.Values);
                sorted_invs.Sort((i1, i2) =>
                {
                    return string.Compare(i1.VariantId, i2.VariantId, true);
                });

                return new Inventory()
                {
                    ProductId = product_id,
                    ImageUrl = store.GetImageUrl(product_id),
                    Variants = sorted_invs.ToArray()
                };
            });
        }

        public Task<Inventory> GetInventoryKRAsync(string product_id)
        {
            throw new NotImplementedException();
        }

#if DEBUG
        static Object _debugLock = new Object();
        static int _gettingCount = 0, _httpCount = 0;
#endif // DEBUG
        public Task<Inventory> GetInventoryAsync(string store_id, string product_id)
        {
#if future
            if (store_id == "jp")
                return GetInventoryJPAsync(product_id);
            else if (store_id == "kr")
                return GetInventoryKRAsync(product_id);
#endif // future

            return Task.Run(async () =>
            {
#if DEBUG
                lock (_debugLock)
                {
                    _gettingCount++;
                    Console.WriteLine(store_id + " " + product_id + " enter");
                }
#endif // DEBUG

                Store store = LookupStore(store_id);
                if (store == null)
                    throw new Exception("unknown store " + store_id);

                Inventory inv = new Inventory()
                {
                    ProductId = product_id,
                    ImageUrl = store.GetImageUrl(product_id)
                };

                List<Variant> variants = new List<Variant>();

                try
                {
#if DEBUGxx
                    if (store_id != "uk" || product_id != "BA8927")
                        throw new Exception();
#endif // DEBUG

#if kill
                    string text = null;
                    for (int retry = 0; retry < 5; retry++)
                    {
                        try
                        {
                            text = await SimpleHttp.GetTextAsync(store.GetVariantUrl(product_id));
                            break;
                        }
                        catch { text = null; }
                    }

                    if (text == null)
                        throw new Exception("HTTP failed");
#else // kill
#if DEBUG
                    lock (_debugLock)
                    {
                        _httpCount++;
                        Console.WriteLine(store_id + " " + product_id + " HTTP enter " + store.GetVariantUrl(product_id));
                    }
#endif // DEBUG

                    Newtonsoft.Json.Linq.JToken json;
                    try
                    {
                        int retry = 0;
                        while (true)
                        {
                            retry++;

                            bool http_ok = false;
                            try
                            {
                                string text = await SimpleHttp.GetTextAsync(store.GetVariantUrl(product_id), TimeSpan.FromSeconds(3));
                                http_ok = true;
                                json = (Newtonsoft.Json.Linq.JToken)Newtonsoft.Json.JsonConvert.DeserializeObject(text);
                                break;
                            }
                            catch
                            {
                                if (!http_ok)
                                    throw;

                                // retry for json error only because sometimes it respond a html instead of json
                                if (retry == 3)
                                    throw;
                            }
                        }
                    }
                    finally
                    {
#if DEBUG
                        lock (_debugLock)
                        {
                            _httpCount--;
                            Console.WriteLine(store_id + " " + product_id + " HTTP leave " + _httpCount);
                        }
#endif // DEBUG
                    }
#endif // kill

                    List<string> in_stock_variants = new List<string>();

                    Newtonsoft.Json.Linq.JToken variation_obj = json.Value<Newtonsoft.Json.Linq.JToken>("variations");
                    Newtonsoft.Json.Linq.JArray variants_obj = variation_obj.Value<Newtonsoft.Json.Linq.JArray>("variants");
                    foreach (Newtonsoft.Json.Linq.JToken variant_obj in variants_obj)
                    {
                        string variant_id = variant_obj.Value<string>("id");

                        int quantity = variant_obj.Value<int>("ATS");

                        if (quantity == 0)
                            continue;
                        in_stock_variants.Add(variant_id);

                        Newtonsoft.Json.Linq.JToken pricing_obj = variant_obj.Value<Newtonsoft.Json.Linq.JToken>("pricing");

                        variants.Add(new Variant()
                        {
                            VariantId = variant_id,
                            Stores = new StoreVariant[]
                            {
                                new StoreVariant()
                                {
                                    Store = store_id,
                                    Size = variant_obj.Value<Newtonsoft.Json.Linq.JToken>("attributes").Value<string>("size"),
                                    SizeUnit = store.SizeUnit,
                                    Quantity = quantity,
                                    Price = pricing_obj.Value<float>("standard"),
                                    PriceSale = pricing_obj["sale"] != null ? pricing_obj.Value<float>("sale") : (float?)null,
                                    Currency = store.Currency,
                                    Dollar = store.Dollar,
                                }
                            }
                        });
                    }

                    Func<string, int, bool> change_quantity = (vid, q) =>
                    {
                        Variant variant = variants.Find(v => { return v.VariantId == vid; });
                        if (variant == null)
                            return false;
                        variant.Stores[0].Quantity = q;
                        return true;
                    };

                    // ugly workaround
                    // sometimes Variation url returns "in stock", but in fact it is not "cartable"
                    foreach (string variant_id in in_stock_variants)
                    {
                        string cart_url = store.GetAddCartUrl(variant_id);
                        string cart_text;
                        try
                        {
                            cart_text = await SimpleHttp.GetTextAsync(cart_url);
                        }
                        catch { throw; }

                        // if there is error, response is a json object otherwise it is a html
                        try
                        {
                            Newtonsoft.Json.Linq.JToken error_obj = (Newtonsoft.Json.Linq.JToken)Newtonsoft.Json.JsonConvert.DeserializeObject(cart_text);
                            string err_msg = error_obj.Value<string>("error");
                            if (err_msg == "INVALID_CAPTCHA")
                            {
                                // require captcha, nothing can be done, just skip checking
                                break;
                            }
                            else if (err_msg == "OUT-OF-STOCK")
                            {
                                change_quantity(variant_id, 0);
                                continue;
                            }

                            // for all other errors, just skip checking
                            break;
                        }
                        catch { }

                        string start_tag = "<div class=\"hidden\" data-component=\"pagecontext/Context\">";
                        string end_tag = "</div>";

                        int start_pos = cart_text.IndexOf(start_tag);
                        int end_pos = start_pos == -1 ? -1 : cart_text.IndexOf(end_tag, start_pos + start_tag.Length);
                        if (start_pos != -1 && end_pos != -1)
                        {
                            start_pos += start_tag.Length;
                            string encoded_json_str = cart_text.Substring(start_pos, end_pos - start_pos);
                            string json_str = System.Web.HttpUtility.UrlDecode(encoded_json_str);
                            Newtonsoft.Json.Linq.JToken cart_json = (Newtonsoft.Json.Linq.JToken)Newtonsoft.Json.JsonConvert.DeserializeObject(json_str);
                            bool available = (cart_json["product_status**"] != null && cart_json.Value<string>("product_status**") == "IN STOCK");
                            if (!available)
                            {
                                change_quantity(variant_id, 0);
                                continue;
                            }

                            // check if the variant is in the available sizes list
                            string[] available_sizes = cart_json.Value<string>("product_sizes**").Split(',');
                            string product_url = cart_json.Value<string>("product_url**");
                            string product_name = cart_json.Value<string>("product_name**");
                            string product_gender = cart_json.Value<string>("product_gender**");
                            foreach (Variant variant in variants)
                            {
                                if (!Array.Exists(available_sizes, s => { return s == variant.Stores[0].Size; }))
                                    variant.Stores[0].Quantity = 0;
                                else
                                {
                                    if (!string.IsNullOrEmpty(product_url))
                                        variant.Stores[0].ProductUrl = product_url;
                                    if (!string.IsNullOrEmpty(product_gender))
                                        variant.Stores[0].Gender = product_gender;
                                    if (!string.IsNullOrEmpty(product_name))
                                        variant.Stores[0].Name = product_name;
                                    variant.Stores[0].AddToCartUrl = store.GetAddCartUrl(variant.VariantId, true);
                                }
                            }

                            break;
                        }
                    }

                    inv.Variants = variants.ToArray();
                }
                catch
                {
                    inv.Variants = new Variant[0];
                }

#if DEBUG
                lock (_debugLock)
                {
                    _gettingCount--;
                    Console.WriteLine(store_id + " " + product_id + " leave " + _gettingCount);
                }
#endif // DEBUG

                return inv;
            });
        }
    }
}
